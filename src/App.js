import React, { BrowserRouter as Router} from 'react';
import './App.css';
import Navbar from "./Navbar";
import PlayerTable from "./PlayerTable";

function App() {
  return (
    <>
    <Navbar />
    <PlayerTable />
    </>
  );
}

export default App;
