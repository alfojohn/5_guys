import React, { useEffect, useRef } from "react";
import data from "./data.json";
import { Grid } from "gridjs-react";

export default function Table() {
    const gridProps = new Grid({
      data: data,
      search: {
        enabled: true,
        //Change keyword to be the name of whoever logged in (grab from form)
        keyword: 'Flynn'
      },
      columns: [
        "Table",
        "Start",
        "Stop",
        "Time",
        "Value",
        "Player",
        "Session",
        "Field",
        "Measure",
        "Host",
        "Topic"
      ],
      pagination: {
        enabled: true,
        limit: 10
      },
      style: {
        table: {
          border: '5px solid #DFFF5a',
          'font-family': 'Franklin Gothic Medium'
        },
        th: {
          'background-color': '#DFFF5a',
          color: '#000',
          'border-bottom': '3px solid #ccc',
          'text-align': 'center'
        },
        td: {
          'text-align': 'center',
          'background-color': '#8cc3de'
        },
        footer: {
          'background-color': '#DFFF5a',
          'font-family': 'Franklin Gothic Medium'
        }
      }
    });
  
    return (
      <div className="grid">
        <h1>Your performance</h1>
        <Grid {...gridProps.props} />
      </div>
    );
  }