import React from 'react';
import './Navbar.css';

const Navbar = () => {
    return (
<nav>
    <ul>
        <li>Players</li>
        <li>Player Performance</li>
        <li>Recent Sessions</li>
        <li>Group Performance</li>
    </ul>
</nav>
    )
}
export default Navbar;