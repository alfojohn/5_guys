const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');
app.use(bodyParser.json());

const con = mysql.createConnection({
  host: "playerperformance.c49mbog42jn4.us-west-1.rds.amazonaws.com",
  user: "admin",
  password: "TI3E9905u52JkYZ4hZFQ",
  database: "playerperformance"
});

con.connect((err) => {
  if (err) throw (err);
  console.log('Connected to database!');
});

app.get('/getDistance', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Distance' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getRunDistance', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Run Distance' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getSprintDistance', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Sprint Distance' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getWorkload', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Workload' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getTotalDistance', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Total Distance' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getTotalRunDistance', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Total Run Distance' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getTotalSprintDistance', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Total Sprint Distance' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getTotalWorkload', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Total Workload' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getLatitude', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Lat' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getLongitude', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Lot' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getHeight', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Height' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.get('/getVelocity', (req, res) => {
  const sqlQuery = "SELECT value FROM playerperformance.playerperformance WHERE field = 'Velocity' LIMIT 10";
  const query = con.query(sqlQuery, (err, result) => {
  if (err) throw (err);
  res.send(result)
});
});

app.listen(3000,() =>{
  console.log("Starting server on port 3000");
});